# express_venus

## 开发环境
`npm install`
`npm start`

## deploy
`cd bin`
`pm2 start www`

## 项目基本使用
bin: 项目启动文件www存放在此文件夹，可以直接在项目根目录执行npm start或者进入目录再执行node www命令来启动项目
contronller: 路由层文件，在路由层接收前端参数，并通过调用service层或者直接调用dao层实现逻辑。
dao: 封装数据库操作，里面包含数据表数据结构定义以及数据库操作，使用orm框架sequelize
db: 主要配置了redis配置，以及token的生成算法
log: 用于存储接口注释信息，执行apidoc的生成文档命令会根据这个文件的注释自动生成在线文档
service: 如果接口逻辑较复杂，则要将业务逻辑提取出来放入service。
util: 通用文件，比如账号密码加密，读取前端传参以及图片上传的封装等通用方法都可以定义在这个目录下
view: 视图层，不需要返回给前端视图，则无需使用本文件
app.js: 项目配置，需要生成新路由则需要在此文件配置路由路径指向contronller层路由文件
config.js: 全局变量定义文件，存储比如数据库配置信息等。
package.json: 安装的npm库版本信息都会在这个目录显示，后期可以直接npm install重装库

## 在线API文档地址
https://mbapi.niyueling.cn/doc/