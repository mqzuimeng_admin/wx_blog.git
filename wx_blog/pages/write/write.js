//logs.js
const app = getApp();
const util = require('../../utils/util.js');
const {
  $Toast
} = require('../../dist/base/index');
Page({
  data: {
    tip: "",
    loading: false,
    result: [],
    checkStatus: true,
		notice_message: '写作功能已经上线，快来愉快写作吧！',
		show: false,
		buttons: [
			{
				type: 'error',
				className: 'screenDialog',
				text: '放弃草稿',
				value: 0
			},
			{
				type: 'primary',
				className: 'screenDialog',
				text: '继续编辑',
				value: 1
			}
		]
  },

	buttontap(e) {
		this.setData({show: false});
		if(e.detail.index == 0) {
			wx.request({
				url: util.basePath + '/article/v1/delDraftContent',
				method: "post",
				data: {
					openid: wx.getStorageSync("loginInfo").openid,
					app_sid: wx.getStorageSync("loginInfo").app_sid
				},
				header: {
					'content-type': 'application/json'
				},
				success(res) {
					if (res.data.status == 200) {
						wx.navigateTo({
							url: '/pages/writearticle/writearticle',
						});
					} else {
						$Toast({
							content: res.data.err,
							type: 'error'
						});
					}
				}
			});
		} else {
			wx.setStorage({
				key: 'isEdit',
				data: true,
			})
			wx.navigateTo({
				url: '/pages/writearticle/writearticle',
			});
		}
	},

  onReady: function() {
    var checkStatus = wx.getStorageSync("checkStatus");

    if (!checkStatus) {
      wx.reLaunch({
        url: '/pages/index/index',
      });
    }
  },

  onLoad: function(options) {
    this.returnNoticeMessage();
    var checkStatus = wx.getStorageSync("checkStatus");
    this.setData({
      checkStatus: checkStatus
    });
  },

  toUsPage: function() {
    wx.navigateTo({
      url: '/pages/logs/logs',
    });
  },

  toWrite: function() {
		var that = this;
    //判断草稿箱是否有文章
    wx.request({
      url: util.basePath + '/article/v1/checkDraftContent',
      method: "post",
      data: {
        openid: wx.getStorageSync("loginInfo").openid,
        app_sid: wx.getStorageSync("loginInfo").app_sid
      },
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          if (res.data.payload.count == 1) {
						that.setData({
							show: true,
							article_title: res.data.payload.title
						})
          } else {
            wx.navigateTo({
              url: '/pages/writearticle/writearticle',
            });
          }
        } else {
          $Toast({
            content: res.data.err,
            type: 'error'
          });
        }
      }
    });
  },

  returnNoticeMessage: function() {
    var that = this;

    wx.request({
      url: util.basePath + '/article/v1/returnNoticeMessage',
      method: "post",
      data: {
        type: '0'
      },
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          that.setData({
            notice_message: res.data.payload
          });
        }
      }
    });
  }
});