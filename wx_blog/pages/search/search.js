const app = getApp();
const util = require('../../utils/util.js');
const {
  $Toast
} = require('../../dist/base/index');
var pageNo = 1;
var keyword = '';
Page({
  data: {
    tip: "",
    loading: false,
    content: []
  },

  onLoad: function(options) {
    this.setData({
      keyword: options.keyword
    });
    this.getIndexList(options.keyword);
  },

  details(e) {
    wx.navigateTo({
      url: '../details/details?id=' + e.currentTarget.id
    });
  },

  onPullDownRefresh: function() {
    wx.showNavigationBarLoading();
    this.data.content = [];
    pageNo = 1;
    this.getIndexList(this.data.keyword);

    setTimeout(function() {
      wx.hideNavigationBarLoading();
      wx.stopPullDownRefresh();
    }, 1500);
  },

  onReachBottom: function() {
    var that = this;
    ++pageNo;
    this.getIndexList(that.data.keyword);
  },

  onShareAppMessage: function() {
    return {
      title: '空城丶Blog',
      path: '/pages/index/index'
    }
  },
  getIndexList: function(keyword) {
    var that = this;
    that.setData({
      loading: true,
      tip: "正在加载"
    });

    wx.request({
      url: util.basePath + '/article/v1/searchArticle',
      method: "post",
      data: {
        keyword: keyword,
        page: pageNo
      },
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          that.setData({
            content: that.data.content.concat(res.data.payload),
            loading: false,
            tip: "加载完成"
          });
        } else {
          that.setData({
            loading: false,
            tip: pageNo == 1 ? '未找到搜索结果!' : '没有更多数据了!'
          });
          $Toast({
            content: pageNo == 1 ? '未找到搜索结果!' : '没有更多数据了!',
            type: 'error'
          });
        }
      }
    });
  }
});