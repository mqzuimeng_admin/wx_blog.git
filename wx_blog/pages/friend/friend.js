const app = getApp();
const util = require('../../utils/util.js');
const {
  $Toast
} = require('../../dist/base/index');
var interval;

Page({
  data: {
    friendphone: '',
    isShowModal: false,
    slideButtons: [{
      text: '备注',
      src: '/images/bz.png',
    }, {
      text: '关心',
      extClass: 'test',
      src: '/images/gx.png',
    }, {
      type: 'warn',
      text: '删除',
      extClass: 'test',
      src: '/images/del.png',
    }],
		buttons: [{ text: '取消' }, { text: '确定' }]
  },

	onLoad: function () {
		var userInfo = wx.getStorageSync("loginInfo");
		var that = this;

		if (!userInfo) {
			wx.reLaunch({
				url: '/pages/userinfo/userinfo',
			});
		} else {
			var checkStatus = wx.getStorageSync("checkStatus");
			this.setData({
				userInfo: userInfo,
				checkStatus: checkStatus,
				isShow: checkStatus ? true : false,
				friendList: wx.getStorageSync("friendList"),
				tip: '已经到底了'
			});

			if (!checkStatus) {
				wx.reLaunch({
					url: '/pages/index/index',
				});
			} 
		}
	},

  toChat: function(e) {
    var that = this;
    var index = e.currentTarget.dataset.current;

    var chatInfo = {
      userInfo: {
        account: that.data.userInfo.account,
        app_sid: that.data.userInfo.app_sid,
        avatar: that.data.userInfo.avatar,
        username: that.data.userInfo.username,
        openid: that.data.userInfo.openid
      },
      friendInfo: {
        account: that.data.friendList[index].friendphone,
        app_sid: that.data.userInfo.app_sid,
        avatar: that.data.friendList[index].avatar,
        username: that.data.friendList[index].username,
        openid: that.data.friendList[index].openid
      }
    }

    that.data.friendList[index].rename ? chatInfo.friendInfo.rename = that.data.friendList[index].rename : '';

    wx.setStorage({
      key: 'chatInfo',
      data: chatInfo,
    });

    //跳转到聊天界面
    wx.navigateTo({
      url: '/pages/chat/chat',
    });
  },

  onShow: function() {
		
  },

  onHide: function() {

  },

  onUnload: function() {

  },

  onPullDownRefresh: function() {
    wx.showNavigationBarLoading();
    this.getFriendList();
    setTimeout(function() {
      wx.hideNavigationBarLoading();
      wx.stopPullDownRefresh()
    }, 1000);
  },

  group: function() {
    $Toast({
      content: '尚待开发!',
      type: 'error'
    });
  },

  goods: function() {
    $Toast({
      content: '尚待开发!',
      type: 'error'
    });
  },

  cancel: function() {
    this.setData({
      isShowModal: !this.data.isShowModal
    });
  },

  commentInput: function(e) {
    this.setData({
      rename: e.detail.value
    });
  },

  showModal: function() {
    this.setData({
      isShowModal: true
    });
  },

  confirm: function(e) {
    var that = this;

		if(e.detail.index == 0) {
			this.setData({
				isShowModal: false
			});
		}

		if (e.detail.index == 1) {
			wx.request({
				url: util.basePath + '/article/v1/renameFriend',
				method: "post",
				data: {
					id: that.data.friend_id,
					rename: that.data.rename
				},
				header: {
					'content-type': 'application/json'
				},
				success(res) {
					if (res.data.status == 200) {
						that.setData({
							isShowModal: !that.data.isShowModal,
							rename: ''
						});
						$Toast({
							content: res.data.payload,
							type: 'success'
						});

						that.getFriendList();
					} else {
						that.setData({
							isShowModal: !that.data.isShowModal,
							rename: ''
						});
						$Toast({
							content: res.data.err,
							type: 'error'
						});
					}
				}
			});
		}
  },

  getFriendInfo: function(e) {
    var that = this;
    var id = e.currentTarget.dataset.index;

		if (e.detail.index == 0) {
			that.setData({
				friend_id: id
			});
			that.showModal();
		}

		if (e.detail.index == 1) {
			that.setFocusFriend(id);
		}

		if (e.detail.index == 2) {
			that.deleteFriend(id);
		}
  },

  setFocusFriend: function(id) {
    var that = this;

    wx.request({
      url: util.basePath + '/article/v1/setFocusFriend',
      method: "post",
      data: {
        id: id
      },
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          $Toast({
            content: res.data.payload,
            type: 'success'
          });

          that.getFriendList();
        } else {
          $Toast({
            content: res.data.err,
            type: 'error'
          });
        }
      }
    });
  },

  deleteFriend: function(id) {
    var that = this;

    wx.request({
      url: util.basePath + '/article/v1/deleteFriend',
      method: "post",
      data: {
        id: id
      },
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          $Toast({
            content: res.data.payload,
            type: 'success'
          });

          that.getFriendList();
        } else {
          $Toast({
            content: res.data.err,
            type: 'error'
          });
        }
      }
    });
  },

  getFriendList: function() {
    var that = this;

    wx.request({
      url: util.basePath + '/article/v1/getFriendList',
      method: "post",
      data: {
        account: that.data.userInfo.account,
        app_sid: that.data.userInfo.app_sid
      },
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          that.setData({
            friendList: res.data.payload,
            tip: '已经到底了'
          });
        } else {
          that.setData({
            tip: '暂无好友'
          });
        }
      }
    });
  },

  onSearch: function(event) {
    var friendphone = event.detail.value;
    var that = this;

    wx.request({
      url: util.basePath + '/article/v1/getFriendInfo',
      method: "post",
      data: {
        account: friendphone,
        app_sid: that.data.userInfo.app_sid
      },
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          wx.setStorage({
            key: 'friendInfo',
            data: res.data.payload
          });

          wx.navigateTo({
            url: '/pages/friendinfo/friendinfo',
          });
        } else {
          $Toast({
            content: res.data.err,
            type: 'error'
          });
        }
      }
    });
  }
});