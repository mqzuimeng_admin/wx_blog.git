//获取应用实例
const app = getApp();
const util = require('../../utils/util.js');
const {
  $Toast
} = require('../../dist/base/index');
var pageNo = 1;
Page({
  data: {
    current: 0, //当前所在滑块的 index
    comment: [],
    indicatorDots: true, //是否显示面板指示点
    tip: "",
    loading: false,
    isShowModal: true,
    article_id: 0
  },

  onLoad: function(options) {
    var that = this;
    var userInfo = wx.getStorageSync("loginInfo");
		var authorInfo = wx.getStorageSync("authorInfo");
    that.setData({
      article_id: options.id,
      userInfo: userInfo,
			authorInfo: authorInfo
    });
    this.getComment(options.id);


  },
  cancel: function() {
    this.setData({
      isShowModal: !this.data.isShowModal
    });
  },
  commentInput: function(e) {
    this.setData({
      article_comment: e.detail.value
    });
  },
	submit_comment: function() {
		this.setData({
			isShowModal: false
		});
  },
  confirm: function() {
    var that = this;

    wx.request({
      url: util.basePath + '/article/v1/addArticleComment',
      method: "post",
      data: {
        account: that.data.userInfo.account,
        app_sid: that.data.userInfo.app_sid,
        comment: that.data.article_comment,
        article_id: that.data.article_id,
        username: that.data.userInfo.username,
        avatar: that.data.userInfo.avatar,
				openid: that.data.authorInfo.author_openid
      },
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          that.setData({
            isShowModal: !that.data.isShowModal,
            article_comment: ''
          });
          $Toast({
            content: '评论成功',
            type: 'success'
          });

					that.getComment(that.data.article_id);
        } else {
          that.cancel();
          $Toast({
            content: res.data.err,
            type: 'error'
          });
        }
      }
    });
  },
  onPullDownRefresh: function() {
    // 显示顶部刷新图标
    wx.showNavigationBarLoading();
    this.data.content = [];
    pageNo = 1;
    this.getIndexList();

    setTimeout(function() {
      // 隐藏导航栏加载框
      wx.hideNavigationBarLoading();
      //停止当前页面下拉刷新。
      wx.stopPullDownRefresh()
    }, 1500)
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this;
    ++pageNo;
    this.getComment();

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function(res) {

  },
  getComment: function(id) {
    //获取评论详情
    var that = this;

    wx.request({
      url: util.basePath + '/article/v1//articleComments/' + id,
      method: "post",
      data: {},
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          that.setData({
            comment: res.data.payload.article
          });
        }
      }
    });
  }
});