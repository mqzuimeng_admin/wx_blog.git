Page({

	data: {
		markers: [],
		polyline: [],
		controls: [{
			id: 1,
			iconPath: '/images/location.png',
			position: {
				left: 0,
				top: 300 - 50,
				width: 50,
				height: 50
			},
			clickable: true
		}]
	},

	onLoad: function (options) {
		var mapInfo = JSON.parse(options.mapInfo);
		this.setData({
			mapInfo: mapInfo,
			markers: [{
				iconPath: "/images/location.png",
				id: 0,
				latitude: mapInfo.target_location.latitude,
				longitude: mapInfo.target_location.longitude,
				width: 50,
				height: 50
			},
				{
					iconPath: "/images/wz.png",
					id: 1,
					latitude: mapInfo.now_location.latitude,
					longitude: mapInfo.now_location.longitude,
					width: 50,
					height: 50
				}],
			polyline: [{
				points: [{
					latitude: mapInfo.target_location.latitude,
					longitude: mapInfo.target_location.longitude
				},
				{
					latitude: mapInfo.now_location.latitude,
					longitude: mapInfo.now_location.longitude
				}],
				color: "#FF0000DD",
				width: 2,
				dottedLine: true,
				arrowLine: true
			}]
		});
	}
});